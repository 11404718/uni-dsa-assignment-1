// Include custom classes.
#include "main.h"

using namespace std;

int main(int argc, char* argv[])
{
   // Declare file and container variables.
   ifstream file;
   container all;
   
   // Exit the program if no input file is stated.
   if (argc < 2)
   {
      cout << "Please enter a file name.\n\n";
      return 0;
   }
   
   // Convert filename into applicable string type, then open the file.
   string fileName = argv[1];
   file.open(fileName.c_str());
   
   // Check that the file is open.
   if (file.is_open())
   {      
      string str;
      
      // Loop through each line of the file until the "END" packet is reached.
      while(getline(file, str))
      {
         if (str.substr(0, 3) != "END")
         {
            // Invoke the container function to add the line to the message/packet list.
            all.add(str);
         }
      }
      
      // Print all messages/packets in the list.
      all.printAll();
      
      // Clear the memory utilised by the list.
      all.freeList();

   }
   else
   {
      // Abort it no file is found.
      cout << "Could not find file '" << fileName << "'. Aborting program.\n";
   }
   
   // Close the file.
   file.close();
   
   return 0;
   
}
