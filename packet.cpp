#include <iostream>
#include "packet.h"

using namespace std;

// Class constructor taking a sequence ID, and raw text.
packet::packet(int sequenceID, string rawPacket)
{
   this->sequenceID = sequenceID;
   text = rawPacket;
}

// Getter for the Sequence ID.
int packet::getSequenceID()
{
   return sequenceID;
}


// Getter for the packet text.
string packet::getText()
{
   return text;
}
