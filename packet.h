#ifndef PACKET_H
#define PACKET_H

#include <iostream>

using namespace std;

class packet
{
   public:
      packet(int sequenceID, string rawPacket); 
      string getText();
      int getSequenceID();
      
   private:
      int sequenceID;
      string text;
};

#endif
