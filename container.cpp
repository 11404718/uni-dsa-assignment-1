#include <iostream>
#include "message.h"
#include "container.h"

// The add function performs a "auto-sorting insertion" for both
// messages and packets.

void container::add(string rawMessage)
{
   // Extract the characteristics of the message
   // from the raw message.
   int messageID = getMessageID(rawMessage);
   int sequenceID = getSequenceID(rawMessage);
   string packetText = getPacketText(rawMessage);
   
   // If the message already exists, add the packet to the 
   // message via message's method.
   if (checkIfExists(messageID)) 
   {
      getMessageFromID(messageID).add(sequenceID, packetText);
   }
   else
   {
      // Add a new message if there isn't one already. Firstly
      // this method will check if there are messages already in
      // the list, and if so will simply create a message and abort.
      // Otherwise, the program will loop through all messages until
      // it finds the place where the new message should go based
      // on the passed ID. Lastly, if the new message has not been
      // inserted at all, it is assumed it belongs at the end of the
      // list and adds it at the end.
      
      int size = messages.size();
   
      if (size == 0)
      {
         messages.push_back(message(messageID, sequenceID, packetText));
      }
      else if (size > 0)
      {
         list<message>::iterator itr;
         bool inserted = false;
      
         for (itr = messages.begin(); itr != messages.end(); itr++)
         {
            message checkID = *itr;
            if (messageID < checkID.getID())
            {
               messages.insert(itr, message(messageID, sequenceID, packetText));
               inserted = true;
               break;
            }
         }
      
         if (!inserted)
         {
            messages.push_back(message(messageID, sequenceID, packetText));
         }
      
      }   

   }
   
}


// Function returning the message with a specific ID.
message& container::getMessageFromID(int checkID)
{
   list<message>::iterator itr;
   
   for (itr = messages.begin(); itr != messages.end(); itr++)
   {
      message tempMessage = *itr;
      if (checkID == tempMessage.getID())
      {
         return *itr;
      }
   }
   
}


// Checking whether a message already exists based on it's ID.
bool container::checkIfExists(int checkID)
{
   list<message>::const_iterator itr;
   
   for (itr = messages.begin(); itr != messages.end(); itr++)
   {
      message tempMessage = *itr;
      if (checkID == tempMessage.getID())
      {
         return true;
      }
   }
   
   return false;
}

// Print all messages and their below packets.
void container::printAll()
{   
   list<message>::const_iterator itr;
   for (itr = messages.begin(); itr != messages.end(); itr++)
   {
      message tempMessage = *itr;
      tempMessage.print();
      cout << "\n";
   }
   
}


// String function to return new message ID based on passed string.
int container::getMessageID(string passedString)
{
   int convertedInt;
   string extractedID = passedString.substr(0, 3);
   
   istringstream(extractedID) >> convertedInt;
   
   return convertedInt;
   
}


// String function to return new sequence ID based on passed string.
int container::getSequenceID(string passedString)
{
   int convertedInt;
   string extractedID = passedString.substr(4, 7);
   
   istringstream(extractedID) >> convertedInt;
   
   return convertedInt;
}


// String function to return new packet text based on passed string.
string container::getPacketText(string passedString)
{
   string extractedText = passedString.substr(8, 90);

   return extractedText;
}

// Free the messages within the message list.
void container::freeList()
{
   messages.clear();
}
