#ifndef CONTAINER_H
#define CONTAINER_H

#include <iostream>
#include <list>
#include <sstream>
#include <stdlib.h>
#include "message.h"

using namespace std;

class container
{   
   public:
      void add(string rawMessage);
      message& getMessageFromID(int checkID);
      bool checkIfExists(int checkID);
      void printAll();
      int getMessageID(string passedString);
      int getSequenceID(string passedString);
      string getPacketText(string passedString);
      void freeList();  
   
   private:
      list<message> messages;
};

#endif
