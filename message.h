#ifndef MESSAGE_H
#define MESSAGE_H

#include <iostream>
#include <list>
#include <sstream>
#include "packet.h"

using namespace std;

class message
{
   public:
      message(int messageID, int sequenceID, string packetText);
      int getID();
      void add(int sequenceID, string packetText);
      void print();
      //bool compareFirst(const packet & a, const packet & b);
      string padZeros(int messageID);
      
   private:
      int ID;
      list<packet> packets;
         
};

#endif
