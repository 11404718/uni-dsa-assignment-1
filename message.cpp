#include <iostream>
#include "message.h"

using namespace std;

// Class constructor, taking in an ID, sequence ID and packet text.
message::message(int messageID, int sequenceID, string packetText)
{
   ID = messageID;
   message::add(sequenceID, packetText);
}


// Getter for ID variable.
int message::getID()
{
   return ID;
}      

// Add packet, similar to the container class add function. This
// automatically sorts the list based on where the packets are 
// added into the packets list.
void message::add(int sequenceID, string packetText)
{
   
   int size = packets.size();
   
   // If no packets are currently in the list.
   if (size == 0)
   {
      packets.push_back(packet(sequenceID, packetText));
   }
   // If there are packets in the list.
   else if (size > 0)
   {
      list<packet>::iterator itr;
      bool inserted = false;
      
      for (itr = packets.begin(); itr != packets.end(); itr++)
      {
         packet checkID = *itr;
         if (sequenceID < checkID.getSequenceID())
         {
            packets.insert(itr, packet(sequenceID, packetText));
            inserted = true;
            break;
         }
      }
      // If it hasn't been inserted yet, assume it belongs at the end of the list.
      if (!inserted)
      {
         packets.push_back(packet(sequenceID, packetText));
      }
      
   }   
   
}


// Print all packets within the message list.
void message::print()
{
   cout << "Message " << padZeros(ID) << "\n";
   
   list<packet>::const_iterator itr;

   for (itr = packets.begin(); itr != packets.end(); itr++)
   {
      packet printingPacket = *itr;
      cout << printingPacket.getText() << "\n";
   }
}


// Add zeroes to message IDs so they will ALWAYs be
// three characters.
string message::padZeros(int messageID)
{
   std::ostringstream messageIDString;
   
   if (messageID < 10)
   {
      messageIDString << "00" << messageID;
   }
   else if (messageID < 100)
   {
      messageIDString << "0" << messageID;
   }
   else
   {
      messageIDString << messageID;
   }
   
   return messageIDString.str();
   
}
